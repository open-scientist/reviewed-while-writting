#

## Description

## Raison d'être

## Usage

- éditer des fichiers en [Markdown](https://docs.gitlab.com/ee/user/markdown.html) via l'éditeur de Gitlab
- quand un fichier a été édité, le sauvegarder ("commit changes")

Il est également possible d'utiliser git en ligne de commande.

## Installation

Ce qui suit crée un projet sur gitlab

- choisir une instance de gitlab
- créer un projet via https://gitlab.com/projects/new

Ce qui suit permet d'avoir accès aux commentaires
- installer le navigateur [Chromium](chromium.org)
- installer l'extension [hypothes.is](https://chrome.google.com/webstore/detail/hypothesis-web-pdf-annota/bjfhmglciegochdpefhhlphglcehbmek)
- créer un [compte hypothes.is](https://hypothes.is/signup)
- accéder au compte des annotations (non public)


